<?php

namespace AppBundle\Controller;

use AppBundle\Service\CommissionScheme\CommissionScheme;
use AppBundle\Service\CommissionScheme\CommissionSchemeFixedPercentWithStep;
use AppBundle\Service\CommissionScheme\CommissionSchemeMixedFixedPriceFixedPercent;
use AppBundle\Service\CommissionScheme\CommissionSchemeWithVariableCommission;
use AppBundle\Service\CommissionScheme\VariableCommission;
use AppBundle\Service\Merchant;
use AppBundle\Service\Payment;
use AppBundle\Service\PaymentTransaction;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }
}

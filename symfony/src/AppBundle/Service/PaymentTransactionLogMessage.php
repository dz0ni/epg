<?php

namespace AppBundle\Service;

/**
 * Class PaymentTransactionLogMessage
 * @package AppBundle\Service
 */
class PaymentTransactionLogMessage
{
    /**
     * @var PaymentTransaction
     */
    private $paymentTransaction;

    /**
     * TransactionLogMessage constructor.
     * @param PaymentTransaction $paymentTransaction
     */
    public function __construct(PaymentTransaction $paymentTransaction)
    {
        $this->paymentTransaction = $paymentTransaction;
    }

    /**
     * @return false|string
     */
    public function getMessageJson()
    {
        return json_encode(
            [
                'MerchantId' => $this->paymentTransaction->getMerchant()->getId(),
                'Transaction amount' => $this->paymentTransaction->getAmount(),
                'Epg revenue' => $this->paymentTransaction->getCommissionAmount(),
                'Merchant revenue' => $this->paymentTransaction->getMerchantAmount(),
            ]
        );
    }
}
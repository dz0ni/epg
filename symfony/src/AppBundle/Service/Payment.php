<?php

namespace AppBundle\Service;

use Monolog\Logger;

/**
 * Class Payment
 * @package AppBundle\Service
 */
class Payment
{
    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var bool
     */
    protected $showLogs = true;

    /**
     * @var PaymentTransaction[]
     */
    protected $paymentTransactions;


    /**
     * Payment constructor.
     * @param Logger $logger
     */
    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param PaymentTransaction $paymentTransaction
     */
    public function process(PaymentTransaction $paymentTransaction)
    {
        $commissionAmount = $paymentTransaction->getMerchant()->getCommissionScheme()->calculateRevenue(
            $paymentTransaction,
            $paymentTransaction->getMerchant()->getCommissionScheme()->isRequiredMerchantTransactionsCount() ?
                $this->getMerchantTransactionsCount($paymentTransaction->getMerchant()) : null
        );
        $paymentTransaction->setMerchantAmount($paymentTransaction->getAmount() - $commissionAmount);
        $paymentTransaction->setCommissionAmount($commissionAmount);

        $this->addPaymentTransaction($paymentTransaction);

        $this->logTransaction($this->getLogMessage($paymentTransaction));
    }

    /**
     * @param PaymentTransaction $paymentTransaction
     */
    protected function addPaymentTransaction(PaymentTransaction $paymentTransaction)
    {
        $this->paymentTransactions[] = $paymentTransaction;
    }

    /**
     * @param string $message
     */
    protected function logTransaction($message)
    {
        if ($this->showLogs) {
            $this->logger->info($message);
        }
    }

    /**
     * @return float|int
     */
    public function getEpgRevenue()
    {
        $totalRevenue = 0;

        foreach ($this->paymentTransactions as $paymentTransaction) {
            $totalRevenue += $paymentTransaction->getCommissionAmount();
        }

        return $totalRevenue;
    }

    /**
     * @param Merchant $merchant
     * @return float|int
     */
    public function getMerchantRevenue(Merchant $merchant)
    {
        $totalRevenue = 0;

        foreach ($this->paymentTransactions as $paymentTransaction) {
            if ($paymentTransaction->getMerchant() === $merchant) {
                $totalRevenue += $paymentTransaction->getMerchantAmount();
            }
        }

        return $totalRevenue;
    }

    /**
     * @param Merchant $merchant
     * @return int
     */
    private function getMerchantTransactionsCount(Merchant $merchant)
    {
        $count = 0;

        if (!empty($this->paymentTransactions)) {
            foreach ($this->paymentTransactions as $paymentTransaction) {
                if ($paymentTransaction->getMerchant() === $merchant) {
                    $count++;
                }
            }
        }

        // add +1 since we process the current transaction
        return $count + 1;
    }

    /**
     * @param Logger $logger
     */
    public function setLogger($logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param PaymentTransaction $paymentTransaction
     * @return false|string
     */
    private function getLogMessage(PaymentTransaction $paymentTransaction)
    {
        $transactionLogMessage = new PaymentTransactionLogMessage($paymentTransaction);

        return $transactionLogMessage->getMessageJson();
    }

    /**
     * @param bool $showLogs
     */
    public function setShowLogs($showLogs)
    {
        $this->showLogs = $showLogs;
    }
}
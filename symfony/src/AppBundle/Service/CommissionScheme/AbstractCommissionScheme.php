<?php

namespace AppBundle\Service\CommissionScheme;

/**
 * Class AbstractCommissionScheme
 * @package AppBundle\Service\CommissionScheme
 */
abstract class AbstractCommissionScheme implements CommissionSchemeInterface
{
    /**
     * @var bool
     */
    protected $isRequiredMerchantTransactionsCount;

    /**
     * @param $percent
     * @param $amount
     * @return float|int
     */
    protected function getPercentCommission($percent, $amount)
    {
        return ($percent / 100) * $amount;
    }

    /**
     * @return bool
     */
    public function isRequiredMerchantTransactionsCount()
    {
        return $this->isRequiredMerchantTransactionsCount;
    }

    /**
     * @param bool $isRequiredMerchantTransactionsCount
     */
    public function setIsRequiredMerchantTransactionsCount($isRequiredMerchantTransactionsCount)
    {
        $this->isRequiredMerchantTransactionsCount = $isRequiredMerchantTransactionsCount;
    }
}
<?php

namespace AppBundle\Service\CommissionScheme;

use AppBundle\Service\PaymentTransaction;

/**
 * Class CommissionSchemeWithVariableCommission
 * @package AppBundle\Service\CommissionScheme
 */
class CommissionSchemeWithVariableCommission extends AbstractCommissionScheme
{
    /**
     * @var VariableCommission[]
     */
    private $variableCommission;

    /**
     * CommissionSchemeWithVariableCommission constructor.
     * @param $variableCommission
     */
    public function __construct($variableCommission)
    {
        $this->variableCommission = $variableCommission;
        $this->isRequiredMerchantTransactionsCount = true;
    }

    /**
     * @param PaymentTransaction $paymentTransaction
     * @param $merchantTransactionsCount
     * @return mixed
     */
    public function calculateRevenue(PaymentTransaction $paymentTransaction, $merchantTransactionsCount)
    {
        $commissionAmount = 0;
        foreach ($this->getVariableCommission() as $variableCommission) {
            if ($merchantTransactionsCount >= $variableCommission->getMinStep() && ($merchantTransactionsCount <= $variableCommission->getMaxStep() || empty($variableCommission->getMaxStep()))) {
                $commissionAmount = $this->getPercentCommission($variableCommission->getPercentPrice(), $paymentTransaction->getAmount());
                break;
            }
        }

        return $commissionAmount;
    }

    /**
     * @return VariableCommission[]
     */
    public function getVariableCommission()
    {
        return $this->variableCommission;
    }

    /**
     * @param VariableCommission[] $variableCommission
     */
    public function setVariableCommission($variableCommission)
    {
        $this->variableCommission = $variableCommission;
    }
}
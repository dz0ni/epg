<?php

namespace AppBundle\Service\CommissionScheme;

use AppBundle\Service\PaymentTransaction;

/**
 * Class CommissionSchemeMixedFixedPriceFixedPercent
 * @package AppBundle\Service\CommissionScheme
 */
class CommissionSchemeMixedFixedPriceFixedPercent extends AbstractCommissionScheme
{
    /**
     * @var float
     */
    private $percentPrice;

    /**
     * @var float
     */
    private $moneyPrice;

    /**
     * CommissionSchemeMixedFixedPriceFixedPercent constructor.
     * @param $percentPrice
     * @param $moneyPrice
     */
    public function __construct($percentPrice, $moneyPrice)
    {
        $this->percentPrice = $percentPrice;
        $this->moneyPrice = $moneyPrice;
        $this->isRequiredMerchantTransactionsCount = false;
    }

    /**
     * @param PaymentTransaction $paymentTransaction
     * @param $merchantTransactionsCount
     * @return mixed
     */
    public function calculateRevenue(PaymentTransaction $paymentTransaction, $merchantTransactionsCount)
    {
        $percentCommission = $this->getPercentCommission($this->getPercentPrice(), $paymentTransaction->getAmount());

        return $percentCommission  > $this->getMoneyPrice() ? $percentCommission : $this->getMoneyPrice();
    }

    /**
     * @return float
     */
    public function getMoneyPrice()
    {
        return $this->moneyPrice;
    }

    /**
     * @param float $moneyPrice
     */
    public function setMoneyPrice($moneyPrice)
    {
        $this->moneyPrice = $moneyPrice;
    }

    /**
     * @return float
     */
    public function getPercentPrice()
    {
        return $this->percentPrice;
    }

    /**
     * @param float $percentPrice
     */
    public function setPercentPrice($percentPrice)
    {
        $this->percentPrice = $percentPrice;
    }
}
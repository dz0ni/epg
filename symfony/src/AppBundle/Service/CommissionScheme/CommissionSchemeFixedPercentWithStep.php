<?php

namespace AppBundle\Service\CommissionScheme;

use AppBundle\Service\PaymentTransaction;

/**
 * Class CommissionSchemeFixedPercentWithStep
 * @package AppBundle\Service\CommissionScheme
 */
class CommissionSchemeFixedPercentWithStep extends AbstractCommissionScheme
{
    /**
     * @var float
     */
    private $percentPrice;

    /**
     * @var int
     */
    private $stepWithZeroRevenue;

    /**
     * CommissionSchemeFixedPercentWithStep constructor.
     * @param $percentPrice
     * @param $stepWithZeroRevenue
     */
    public function __construct($percentPrice, $stepWithZeroRevenue)
    {
        $this->percentPrice = $percentPrice;
        $this->stepWithZeroRevenue = $stepWithZeroRevenue;
        $this->isRequiredMerchantTransactionsCount = true;
    }

    /**
     * @param PaymentTransaction $paymentTransaction
     * @param int $merchantTransactionsCount
     * @return mixed
     */
    public function calculateRevenue(PaymentTransaction $paymentTransaction, $merchantTransactionsCount)
    {
        $commissionAmount = 0;
        $commission = $this->getPercentCommission($this->getPercentPrice(), $paymentTransaction->getAmount());

        if ($merchantTransactionsCount % $this->getStepWithZeroRevenue() !== 0) {
            $commissionAmount = $commission;
        }

        return $commissionAmount;
    }

    /**
     * @return float
     */
    public function getPercentPrice()
    {
        return $this->percentPrice;
    }

    /**
     * @param float $percentPrice
     */
    public function setPercentPrice($percentPrice)
    {
        $this->percentPrice = $percentPrice;
    }

    /**
     * @return int
     */
    public function getStepWithZeroRevenue()
    {
        return $this->stepWithZeroRevenue;
    }

    /**
     * @param int $stepWithZeroRevenue
     */
    public function setStepWithZeroRevenue($stepWithZeroRevenue)
    {
        $this->stepWithZeroRevenue = $stepWithZeroRevenue;
    }
}
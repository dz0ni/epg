<?php

namespace AppBundle\Service\CommissionScheme;

use AppBundle\Service\PaymentTransaction;

/**
 * Interface CommissionSchemeInterface
 * @package AppBundle\Service\CommissionScheme
 */
interface CommissionSchemeInterface
{
    /**
     * @param PaymentTransaction $paymentTransaction
     * @param int $merchantTransactionsCount
     * @return float|int
     */
    public function calculateRevenue(PaymentTransaction $paymentTransaction, $merchantTransactionsCount);

    /**
     * @return bool
     */
    public function isRequiredMerchantTransactionsCount();


    /**
     * @param bool $isRequiredMerchantTransactionsCount
     */
    public function setIsRequiredMerchantTransactionsCount($isRequiredMerchantTransactionsCount);
}
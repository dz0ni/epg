<?php

namespace AppBundle\Service\CommissionScheme;

/**
 * Class VariableCommission
 * @package AppBundle\Service\CommissionScheme
 */
class VariableCommission
{
    /**
     * @var int
     */
    private $minStep;

    /**
     * @var int
     */
    private $maxStep;

    /**
     * @var float
     */
    private $percentPrice;

    /**
     * VariableCommission constructor.
     * @param $min
     * @param $max
     * @param $percentPrice
     */
    public function __construct($min, $max, $percentPrice)
    {
        $this->minStep = $min;
        $this->maxStep = $max;
        $this->percentPrice = $percentPrice;
    }

    /**
     * @return mixed
     */
    public function getMinStep()
    {
        return $this->minStep;
    }

    /**
     * @param mixed $minStep
     */
    public function setMinStep($minStep)
    {
        $this->minStep = $minStep;
    }

    /**
     * @return mixed
     */
    public function getMaxStep()
    {
        return $this->maxStep;
    }

    /**
     * @param mixed $maxStep
     */
    public function setMaxStep($maxStep)
    {
        $this->maxStep = $maxStep;
    }

    /**
     * @return mixed
     */
    public function getPercentPrice()
    {
        return $this->percentPrice;
    }

    /**
     * @param mixed $percentPrice
     */
    public function setPercentPrice($percentPrice)
    {
        $this->percentPrice = $percentPrice;
    }

}
<?php

namespace AppBundle\Service;

use AppBundle\Service\CommissionScheme\CommissionSchemeInterface;

/**
 * Class Merchant
 * @package AppBundle\Service
 */
class Merchant
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var CommissionSchemeInterface
     */
    protected $commissionScheme;

    /**
     * Merchant constructor.
     * @param string $id
     * @param CommissionSchemeInterface $commissionScheme
     */
    public function __construct($id, CommissionSchemeInterface $commissionScheme)
    {
        $this->id = $id;
        $this->commissionScheme = $commissionScheme;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return CommissionSchemeInterface
     */
    public function getCommissionScheme()
    {
        return $this->commissionScheme;
    }

    /**
     * @param CommissionSchemeInterface $commissionScheme
     */
    public function setCommissionScheme($commissionScheme)
    {
        $this->commissionScheme = $commissionScheme;
    }
}
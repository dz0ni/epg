<?php

namespace AppBundle\Service;

/**
 * Class PaymentTransaction
 * @package AppBundle\Service
 */
class PaymentTransaction
{
    /**
     * @var float
     */
    private $amount;

    /**
     * @var Merchant
     */
    private $merchant;

    /**
     * @var float
     */
    private $commissionAmount;

    /**
     * @var float
     */
    private $merchantAmount;

    /**
     * PaymentTransaction constructor.
     * @param float $amount
     * @param Merchant $merchant
     */
    public function __construct($amount, Merchant $merchant)
    {
        $this->amount = $amount;
        $this->merchant = $merchant;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return Merchant
     */
    public function getMerchant()
    {
        return $this->merchant;
    }

    /**
     * @param Merchant $merchant
     */
    public function setMerchant($merchant)
    {
        $this->merchant = $merchant;
    }

    /**
     * @return float
     */
    public function getCommissionAmount()
    {
        return $this->commissionAmount;
    }

    /**
     * @param float $commissionAmount
     */
    public function setCommissionAmount($commissionAmount)
    {
        $this->commissionAmount = $commissionAmount;
    }

    /**
     * @return float
     */
    public function getMerchantAmount()
    {
        return $this->merchantAmount;
    }

    /**
     * @param float $merchantAmount
     */
    public function setMerchantAmount($merchantAmount)
    {
        $this->merchantAmount = $merchantAmount;
    }
}
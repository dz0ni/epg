<?php

namespace Tests\AppBundle\Service;

use AppBundle\Service\CommissionScheme\CommissionSchemeFixedPercentWithStep;
use AppBundle\Service\CommissionScheme\CommissionSchemeMixedFixedPriceFixedPercent;
use AppBundle\Service\CommissionScheme\CommissionSchemeWithVariableCommission;
use AppBundle\Service\CommissionScheme\VariableCommission;
use AppBundle\Service\Merchant;
use AppBundle\Service\Payment;
use AppBundle\Service\PaymentTransaction;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PaymentTest extends WebTestCase
{
    public static $payment;

    public function setUp()
    {
        //start the symfony kernel
        $kernel = static::createKernel();
        $kernel->boot();

        static::$payment = $kernel->getContainer()->get(Payment::class);
    }

    public function testProcess()
    {
        /** @var Payment $payment */
        $payment = static::$payment;
        $payment->setShowLogs(true);

        $commissionSchemeA = new CommissionSchemeFixedPercentWithStep(5, 3);
        $commissionSchemeB = new CommissionSchemeMixedFixedPriceFixedPercent(3, 2.25);
        $commissionSchemeC = new CommissionSchemeWithVariableCommission([
            new VariableCommission(0, 10, 5),
            new VariableCommission(11, 50, 2.5),
            new VariableCommission(51, null, 1),
        ]);

        $merchantA = new Merchant('A', $commissionSchemeA);
        $merchantB = new Merchant('B', $commissionSchemeB);
        $merchantC = new Merchant('C', $commissionSchemeC);

        $arrayMerchants = [$merchantA, $merchantB, $merchantC];

        $randomLoops = mt_rand(50, 150);
        $totalAmount = 0;
        for ($i = 0; $i < $randomLoops; $i++) {
            $amount = mt_rand(1, 5000);
            $totalAmount += $amount;
            $payment->process(new PaymentTransaction($amount, $arrayMerchants[array_rand($arrayMerchants)]));
        }

        fwrite(STDERR, print_r('EPG total revenue: ' . $payment->getEpgRevenue() . PHP_EOL, TRUE));
        fwrite(STDERR, print_r('MerchantA income: ' . $payment->getMerchantRevenue($merchantA) . PHP_EOL, TRUE));
        fwrite(STDERR, print_r('MerchantB income: ' . $payment->getMerchantRevenue($merchantB) . PHP_EOL, TRUE));
        fwrite(STDERR, print_r('MerchantC income: ' . $payment->getMerchantRevenue($merchantC) . PHP_EOL, TRUE));

        $this->assertEquals(
            $totalAmount,
            $payment->getEpgRevenue() + $payment->getMerchantRevenue($merchantA) + $payment->getMerchantRevenue($merchantB) + $payment->getMerchantRevenue($merchantC)
        );
    }

}
